import { Component } from '@angular/core';
import { User } from './address-card/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user:User;
  isCollapsed: boolean=true;
  inputText : string = "Initial Value";
  
  constructor(){
    this.user = new User();
    this.user.name="Foo Bar";
    this.user.designation="Software";
    this.user.address="1234";
    this.user.phone=['12345','456788'];
  }
}
