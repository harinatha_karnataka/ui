import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-new-component',
  template:  `
   <h1>Hey guys!</h1>
  <ul>
    <li *ngFor="let arr of myArr">{{ arr }}</li>
  </ul>`,
  styleUrls: ['./my-new-component.component.css']
})
export class MyNewComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

   myArr = ['him','hers','yours','theirs'];
}
