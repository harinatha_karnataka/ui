Need to install the following dependencies

 1.Node.js with NPM (Node Package Manager)
 2.Angular-CLI (Command Line Interface)

After install the node js run the below commond 

> npm install -g @angular/cli

Once finished, type:

> ng -v
The resulting output will look something like this:

    _                      _                 ____ _     ___ 
   / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
  / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | | 
 / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | | 
/_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
               |___/                                        
@angular/cli: 1.0.0                                  
node: 6.10.0                                              
os: win32 x64     


Create the project using the following commnd

> ng new my-project-name

then go to project folder

cd my-project-name

Then install the Type script using following 

npm install typescript@2.2.1 --save

Then start6 the > npm start

we will get the url and started server the url is 

http://localhost:4200/ this is configured in protractor.conf.js file

The above thigs are first / demo files .

Create the Components :-
==========================

// Imports

import { Component } from '@angular/core';

// Component Decorator

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// Component Class
export class AppComponent {
  title = 'app works!';
}

1. Component Imports
import { Component } from '@angular/core';
The way you make a class a component is by importing the Component member from the @angular/core library. 

Some components may have dozens of imports based on the needs of the component. This is also where you import any services that your component may use through dependency injection.

2. The Component Decorator
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
The next section is what's known as the Component Decorator. The @Component is using Component that was imported from the above import line. 

This is what makes this given class a component. 

Within the component, you have a variety of configuration properties that help define this given component. 

selector: This is the name of the tag that the component is applied to. For instance: <app-root>Loading...</app-root> within index.html.
templateUrl & styleUrls: These define the HTML template and stylesheets associated with this component. You can also use template and styles properties to define inline HTML and CSS.
There are other properties you can define within the component decorator depending on the needs of your component. It is also where you define animations.

3. The Component Class
export class AppComponent {
  title = 'app works!';
}
Finally, we have the core of the component, which is the component class. 

This is where the various properties and methods are defined. Any properties and methods defined here are accessible from the template. And likewise, events that occur in the template are accessible within the component.

It's also where dependency injection occurs within a constructor, which gives you access to various services.

Let's make our own component with the help of the CLI.


> ng g component my-new-component

1.It creates a new folder inside of the /app folder based on the name you gave the component.
2.Inside, it generates 4 files: CSS, HTML, TS (component class) and a .spec.ts file for unit tests.
3.Inside /src/app/app.module.ts it imports the new component, and adds it to the declarations.

need to add the following " bootstrap: [AppComponent,MyNewComponentComponent] " in spp.module.ts (append the new component name )

add the following things in index.html to display the new component details 

<body>
  <app-root></app-root>
  <app-my-new-component></app-my-new-component>
</body>

" <app-my-new-component></app-my-new-component> " this added to the existing one in body then both the components dispays

This is avialable in "my-new-component.component.ts " which is newly created component.


we can define the templates in the following way 

template: `
  <h1>Hey guys!</h1>
  <ul>
    <li *ngFor="let arr of myArr">{{ arr }}</li>
  </ul>
  `,
  
  in the component decarator .
  










