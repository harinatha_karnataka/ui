import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Dividend from './Dividend';
import registerServiceWorker from './registerServiceWorker';

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<Dividend />, document.getElementById('root'));
registerServiceWorker();
